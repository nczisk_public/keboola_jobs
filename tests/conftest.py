import pytest
import json

def pytest_addoption(parser):
    parser.addoption("--config", action="store")

@pytest.fixture(scope='session')
def config(request):
    config_value = request.config.option.config
    if config_value is None:
        pytest.skip()
    with open(config_value, 'r') as fp:
        return json.load(fp)



