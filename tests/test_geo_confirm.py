import requests_mock
from sftp_jobs.telco import  Keboola
from sftp_jobs.jobs import JobGeoConfirm



confirm_map = \
    '8a345b42-46cc-48ab-9b6e-5779691fb824,"https://secure.clevermaps.io/#/ndb42s0jel66vd3q/map/view?shareId=8a345b4246cc48ab"\n' \
    '570448b5-6488-4727-bc7d-cd18b46c722a,"https://secure.clevermaps.io/#/ndb42s0jel66vd3q/map/view?shareId=570448b564884727"\n' \
    '16233cc9-1d04-47e0-8b25-873260a0a90c,"https://secure.clevermaps.io/#/ndb42s0jel66vd3q/map/view?shareId=16233cc91d0447e0"\n'


def test_prepare_confirm(config):
    keboola = Keboola(config)
    keboola.create_geo_confirm(confirm_map)


def test_geo(config):
    with requests_mock.Mocker() as m:
        m.post( config['api']['map_confirm_api'] , text='')
        job = JobGeoConfirm(config)
        rt = job._run_implementation()
        assert rt == 3