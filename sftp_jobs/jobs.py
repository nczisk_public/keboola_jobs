import logging
import json
from abc import ABC, abstractmethod
from sftp_jobs.utils import LOG_APP, perf
from sftp_jobs.telco import *

LOG = logging.getLogger(LOG_APP)


class Job(ABC):

    def __init__(self, conf):
        self.conf = conf

    @abstractmethod
    def _run_implementation(self):
        pass

    @perf
    def run(self):
        try:
            self._run_implementation()
        except Exception as e:
            LOG.error(e, exc_info=True)


class JobWhitelists(Job):
    def __init__(self, conf):
        super().__init__(conf)
        # initialize operators
        self.operators = Operator.initialize_all_operators(conf)

    def _run_implementation(self):
        rt = {}
        LOG.debug('Processing whitelists start')
        api = Api(self.conf)
        for top in self.operators:
            try:
                LOG.debug('Processing whitelists for {}'.format(top.op_name))
                j_data = api.get_whitelists(top.op_name)
                if not j_data:
                    LOG.debug('No whitelist entries')
                    continue
                LOG.info("Have {} whitelists entries".format(len(j_data)))
                LOG.debug('Whitelist data {}'.format(j_data))
                rt[top.op_name] = top.create_whitelist(j_data)
            except Exception as e:
                LOG.error(e)
        LOG.debug('Processing whitelists finish')
        return rt


class JobGeo(Job):
    def __init__(self, conf):
        super().__init__(conf)
        # initialize operators
        self.operators = Operator.initialize_all_operators(conf)
        # initialize keboola
        self.keboola = Keboola(conf)

    def _run_implementation(self):
        ret = []
        LOG.debug('Processing geo')
        api = Api(self.conf)
        for top in self.operators:
            LOG.debug('Processing geo for {}'.format(top.op_name))
            nmap = {}
            geo_list  = top.get_all_geo()
            if not geo_list:
                continue
            # substitute
            for file in geo_list:
                errors = 0
                LOG.info('Processing file {} with {} entries'.format(file, len(geo_list[file])))
                LOG.debug('Geo file {} contains  {}'.format(file, geo_list[file]))
                for line in  geo_list[file]:
                    try:
                        # ignore header
                        if line.lower().startswith('dt,period') or line.lower().startswith('"dt","period"'):
                            continue
                        parts = line.rsplit(',', maxsplit=1)
                        if len(parts) != 2:
                            LOG.error('There is problem with line {}'.format(line))
                        else:
                            multipass = api.get_multipass(parts[1])
                            if not multipass:
                                LOG.error('Failed to map number {}'.format(parts[1]))
                            else:
                                s = '{},{}'.format(parts[0], multipass)
                                nmap[s] = s
                    except Exception as e:
                        LOG.error(e)
                        errors += 1
                if errors == 0:
                    os.remove(file)
                    LOG.debug('Removing {}'.format(file))
            LOG.info('Have {} unique mapped entries'.format(len(nmap.values())))
            LOG.debug('Mapped entries {}'.format(nmap.values()))
            ret.append(self.keboola.create_geo(nmap.values(),top.op_name))
        LOG.debug('Processing geo finish')
        return ret


class JobGeoConfirm(Job):
    def __init__(self, conf):
        super().__init__(conf)
        # initialize keboola
        self.keboola = Keboola(conf)

    def _run_implementation(self):
        LOG.debug('Processing geoconfirm')
        api = Api(self.conf)
        clist = self.keboola.get_all_geo_confirm()
        succ = 0
        for file in clist:
            errors = 0
            LOG.debug('Geoconfirm file {} with entries {}'.format(file, clist[file]))
            for line in clist[file]:
                try:
                    # ignore header
                    if line.lower().startswith('multipass') or \
                       line.lower().startswith('"multipass"') or \
                       line.lower().startswith('cislo') or \
                       line.lower().startswith('"cislo"'):
                       continue
                    parts = line.split(',')
                    if len(parts) != 2:
                        LOG.error('There is problem with line {}'.format(line))
                    else:
                        api.map_confirm(parts[0].replace('"',''),parts[1].replace('"',''))
                        succ += 1
                except Exception as e:
                    LOG.error(e)
                    errors += 1
            if errors == 0:
                os.remove(file)
                LOG.debug('Removing {}'.format(file))
        LOG.info('Sent {} confirmations'.format(succ))
        return succ


class JobGeoRemove(Job):
    def __init__(self, conf):
        super().__init__(conf)
        # initialize keboola
        self.keboola = Keboola(conf)

    def _run_implementation(self):
        LOG.debug('Processing georemove')
        rt = None
        api = Api(self.conf)
        try:
            j_data = api.get_ConsentWithdrawn()
            if not j_data:
                LOG.debug('No ConsentWithdrawn entries')
                return rt
            LOG.info("Have {} ConsentWithdrawn entries".format(len(j_data)))
            LOG.debug('ConsentWithdrawn data {}'.format(j_data))
            rt = self.keboola.create_geo_remove(j_data)
        except Exception as e:
            LOG.error(e)
        LOG.debug('Processing georemove finish')
        return rt