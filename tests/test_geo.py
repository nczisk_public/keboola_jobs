import os
import requests_mock
import json
from sftp_jobs.telco import Operator, Keboola
from sftp_jobs.utils import setup_log
from sftp_jobs.jobs import JobGeo


def test_logs(config):
    setup_log(config)


def test_folders(config):
    # check operators folders
    Operator.initialize_all_operators(config, check_and_create_paths=True)
    # check keboola folders
    Keboola(config, check_and_create_paths=True)


geo_pair = ['+421905123456', '7883b4a6-a041-41a7-b4d3-5fefd44e00ae']

geo_timestamps = 'dt,period,lat,lon,precision_code,intensity,id,multipass\n' \
                 '2020-03-09T00:00:00.000Z,80,50.1248,14.4548,1,1.0,2837e3044979c03ae39a343e514fc40d389a16b5,{}\n' \
                 '2020-03-09T00:00:00.000Z,81,50.0473,14.4545,0,0.2,e9091daae681bf128a39df1759bd4004b5215885,{}\n'.format(geo_pair[0],
                                                                                                                          geo_pair[0])

geo_multipass = {
        'warnings': [], 'errors': [], 'info': [],
        'payload': [{'vMultipass': geo_pair[1]}],
        'warning_count': 0, 'error_count': 0, 'payload_count': 1}



geo_result = 'dt,period,lat,lon,precision_code,intensity,id,multipass\n' \
             '2020-03-09T00:00:00.000Z,80,50.1248,14.4548,1,1.0,2837e3044979c03ae39a343e514fc40d389a16b5,{}\n' \
             '2020-03-09T00:00:00.000Z,81,50.0473,14.4545,0,0.2,e9091daae681bf128a39df1759bd4004b5215885,{}\n'.format(geo_pair[1],
                                                                                                                      geo_pair[1])

def test_prepare_geo(config):
    operators = Operator.initialize_all_operators(config, check_and_create_paths=True)
    recipent = config['gpg']['private_recipient']
    for ope in operators:
        ope.create_geo(geo_timestamps, recipent)


def _buff_from_csv(fname):
    with open(fname, 'r') as f:
        lines = f.readlines()
        buff = ""
        for x in lines:
            buff += x
        return buff


def test_geo(config):
    with requests_mock.Mocker() as m:
        m.get( config['api']['multipass_api'] , text=json.dumps(geo_multipass))
        job = JobGeo(config)
        rt = job._run_implementation()
        assert rt
        assert len(rt) == 4
        for fname in rt:
            rt_str = _buff_from_csv(fname)
            assert rt_str == geo_result
