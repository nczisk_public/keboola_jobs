# keboola_jobs

## Required 

python 3.6, gpg

## Instalacia/upgrade

Instalacia venv (ak nie je)

`pip install virtualenv`

Vytvorenie virtualenv

`virtualenv venv`

Aktivacia virtualenv

`source venv/bin/activate`

Instalacia sw (priklad pre verziu 0.0.7)

`pip install -e git+https://gitlab.com/nczisk_public/keboola_jobs.git@0.0.7#egg=keboola_jobs`

Pre upgrade staci pouzit inu verziu  napr.
`pip install -e git+https://gitlab.com/nczisk_public/keboola_jobs.git@0.0.8#egg=keboola_jobs --upgrade`


## Konfiguracia

Template konfiguracie sa nachadza. Pouzijeme ak instalujeme prvy krat.
 
`venv/src/keboola-jobs/sftp_jobs/config_templates/config.json`

Pri prvej instalacii skopirujeme do konfig adresara

`mkdir ~/sftp_jobs
cp  ./venv/src/keboola-jobs/sftp_jobs/config_templates/config.json ~/sftp_jobs
`

Nasledne editovat tuto konfiguraciu


## Beh

`./sftp_jobs_runner

usage: sftp_jobs_runner [-h] --config CONFIG --job
                        [{all,whitelists,geo,geo_confirm}]`
                        
--config - plna cesta na konfiguracny subor
 
--job - aky job chceme spustit, vhodne ak chceme spustat samostane kazdu funkcionalitu. 

Ak chceme vsetko tak all (spusti sa v poradi whitelists,geo,geo_confirm )
* whitelists - vygeneruje whitelisty pre operatorov
* geo - preberie geo data od operatorv a zlucene data posunie pre keboola
* geo_confirm - zoberie mapove linky od keboola a posunie ich cez api do mojeezdravie


Logy behu aplikacie su generovane v suboroch s rotaciou podla konfigu
                        