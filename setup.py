import os
from setuptools import setup

setup(
    name='keboola_jobs',
    version='0.2.2',
    packages=['sftp_jobs'],
    url='https://gitlab.com/nczisk_public/keboola_jobs',
    classifiers=['Programming Language :: Python :: 3',
                 'Programming Language :: Python :: 3.6',
                 'Intended Audience :: Developers',
                 'Environment :: Console',
                 ],

    platforms=['Any'],
    install_requires=['requests', 'python-gnupg'],
    license='MIT License',
    author='rho',
    author_email='richard.holly@nczisk.sk',
    description='',
    include_package_data=True,
    package_data={
        'sftp_jobs': ['config_templates/*'],
    },
    entry_points={
        'console_scripts':['sftp_jobs_runner = sftp_jobs.runner:main']
    }
)
