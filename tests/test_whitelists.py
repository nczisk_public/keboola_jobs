import os
import requests_mock
import json
from sftp_jobs.telco import Operator, Keboola
from sftp_jobs.utils import setup_log
from sftp_jobs.jobs import JobWhitelists




# whitelists = '["+421903111222", "+421903333444", "+421941234567"]'
whitelists =  {
                    'warnings': [], 'errors': [], 'info': [],
                    'warning_count': 0, 'error_count': 0, 'payload_count': 3,
                    'payload':
                    [{'vPhoneNumber': '+421915877208'}, {'vPhoneNumber': '+421905012327'}, {'vPhoneNumber': '+421903457966'}]
               }


def test_whitelists(config):
    tlist = Operator.list_from_rt_json(whitelists)
    with requests_mock.Mocker() as m:
        m.post( config['api']['whitelist_api'] , text=json.dumps(whitelists))
        job = JobWhitelists(config)
        rt = job._run_implementation()
        for top in job.operators:
            tname = top.op_name
            assert rt[tname]
            assert os.path.exists(rt[tname])
            f = rt[tname]
            if f.lower().endswith('.pgp') or f.lower().endswith('.gpg'):
                # decrypt
                decrypted, data = top.decrypt(rt[tname])
                assert decrypted
                dlist = data.splitlines()
                assert  dlist == tlist
            elif f.lower().endswith('csv'):
                with open(f, "r") as fp:
                    unecrypted = fp.read()
                    assert unecrypted
                    dlist = unecrypted.splitlines()
                    assert dlist == tlist

