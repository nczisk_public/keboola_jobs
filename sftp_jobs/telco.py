import os
import logging
import time
import gnupg
import requests
import datetime
import json
from sftp_jobs.utils import LOG_APP, perf, check_path, base_dir

LOG = logging.getLogger(LOG_APP)



class Api:

    def __init__(self, conf):
        self.conf = conf
        self.bearer = conf['api']['bearer']
        self.whitelist_api = conf['api']['whitelist_api']
        self.multipass_api = conf['api']['multipass_api']
        self.map_confirm_api = conf['api']['map_confirm_api']
        self.multipass_for_withdrawn_consents_api = conf['api']['multipass_for_withdrawn_consents_api']

    @perf
    def get_whitelists(self, vTelcoOperatorUUID ):
        checkpoint = datetime.datetime.now()
        params = {
            'vTelcoOperatorUUID': vTelcoOperatorUUID,
            'dDateConfirmedPositiveFrom': self.getlastruntimestamp(vTelcoOperatorUUID)
        }
        headerz = {}
        if self.bearer:
            headerz['Authorization'] =  "Bearer {}".format(self.bearer)
        LOG.debug('Headers {}'.format(headerz))
        resp = requests.post(self.whitelist_api, json=params, headers=headerz)
        if resp.status_code != 200:
            LOG.error('{}{}'.format(resp.status_code, resp.reason))
            return None
        self.savelastruntimestamp(vTelcoOperatorUUID,checkpoint)
        return resp.json()

    @perf
    def get_multipass(self, vPhoneNumber ):
        params = {
            'vPhoneNumber': vPhoneNumber
        }
        headerz = {}
        if self.bearer:
            headerz['Authorization'] =  "Bearer {}".format(self.bearer)
        LOG.debug('Headers {}'.format(headerz))
        resp = requests.get(self.multipass_api, params=params, headers=headerz)
        if resp.status_code != 200:
            LOG.error('{}{}'.format(resp.status_code, resp.reason))
            return None
        rt = resp.json()
        if 'payload' in rt and len(rt['payload']) == 1:
            p0 = rt['payload'][0]
            if 'vMultipass' in p0:
                return p0['vMultipass']
            else:
                return None
        else:
            return None

    @perf
    def map_confirm(self, vMultipass, vURL ):
        params = {
            'vMultipass': vMultipass,
            'vURL': vURL
        }
        headerz = {}
        if self.bearer:
            headerz['Authorization'] =  "Bearer {}".format(self.bearer)
        LOG.debug('Headers {}'.format(headerz))
        resp = requests.post(self.map_confirm_api, json=params, headers=headerz)
        if resp.status_code != 200:
            LOG.error('{}{}'.format(resp.status_code, resp.reason))
            return None
        return True


    @perf
    def get_ConsentWithdrawn(self):
        checkpoint = datetime.datetime.now()
        params = {
            'dDateConsentWithdrawnFrom': self.getConsentWithdrawntimestamp()
        }
        headerz = {}
        if self.bearer:
            headerz['Authorization'] =  "Bearer {}".format(self.bearer)
        LOG.debug('Headers {}'.format(headerz))
        resp = requests.post(self.multipass_for_withdrawn_consents_api, json=params, headers=headerz)
        if resp.status_code not in [200,204]:
            LOG.error('{}{}'.format(resp.status_code, resp.reason))
            return None
        self.saveConsentWithdrawntimestamp(checkpoint)
        rt = resp.json()
        LOG.debug(rt)
        return rt

    @staticmethod
    def getlastruntimestamp( vTelcoOperatorUUID ):
        base = base_dir()
        lstatus_name = os.path.join(base,'{}_last_whitelists.json'.format(vTelcoOperatorUUID))
        if os.path.exists(lstatus_name):
            with open(lstatus_name,'r') as f:
                jdata = json.load(f)
                dt = datetime.datetime.fromtimestamp(jdata['timestamp'])
                return dt.isoformat()
        else:
            return (datetime.datetime.now() - datetime.timedelta(days=1*365)).isoformat()

    @staticmethod
    def savelastruntimestamp( vTelcoOperatorUUID, checkpoint ):
        base = base_dir()
        lstatus_name = os.path.join(base,'{}_last_whitelists.json'.format(vTelcoOperatorUUID))
        jdata = {
            'timestamp': checkpoint.timestamp()
        }
        with open(lstatus_name,'w') as f:
             json.dump(jdata,f)


    @staticmethod
    def getConsentWithdrawntimestamp( ):
        base = base_dir()
        lstatus_name = os.path.join(base,'last_ConsentWithdrawn.json')
        if os.path.exists(lstatus_name):
            with open(lstatus_name,'r') as f:
                jdata = json.load(f)
                dt = datetime.datetime.fromtimestamp(jdata['timestamp'])
                return dt.isoformat()
        else:
            return (datetime.datetime.now() - datetime.timedelta(days=1*365)).isoformat()

    @staticmethod
    def saveConsentWithdrawntimestamp(checkpoint ):
        base = base_dir()
        lstatus_name = os.path.join(base,'last_ConsentWithdrawn.json')
        jdata = {
            'timestamp': checkpoint.timestamp()
        }
        with open(lstatus_name,'w') as f:
             json.dump(jdata,f)

class Operator:

    def __init__(self, conf, op_name, check_and_create_paths = False):
        self.conf = conf
        self.op_name = op_name
        self.home_path = conf['operators'][op_name]['home_path']
        check_path(self.home_path,check_and_create_paths )
        self.whitelists_path = os.path.join(self.home_path,'whitelists')
        check_path(self.whitelists_path, check_and_create_paths)
        self.geo_path =  os.path.join(self.home_path,'geo')
        check_path(self.geo_path, check_and_create_paths)
        self.gpg_recipient = conf['operators'][op_name]['gpg_recipient']
        if not conf['gpg']['gpgbinary'] or len(conf['gpg']['gpgbinary']) == 0:
            self.gpg = gnupg.GPG()
        else:
            self.gpg = gnupg.GPG(gpgbinary=conf['gpg']['gpgbinary'])

    @staticmethod
    def initialize_all_operators(conf, check_and_create_paths = False):
        rt = []
        for x in conf['operators']:
            rt.append(Operator(conf,x, check_and_create_paths))
        return rt

    @staticmethod
    def list_from_rt_json(json_data):
        rt = []
        for x in json_data['payload']:
            rt.append(x['vPhoneNumber'])
        return rt

    @perf
    def create_whitelist(self, json_data):
        t = time.localtime()
        timestamp = time.strftime('%Y%m%d%H%M%S', t)
        fname = os.path.join(self.whitelists_path,'whitelist_'+timestamp+'.csv')
        buff = ""
        for x in self.list_from_rt_json(json_data):
            buff += x + '\n'
        if self.gpg_recipient and len(str(self.gpg_recipient).strip()) > 0:
            fname = fname + '.pgp'
            result = self.encrypt(buff, self.gpg_recipient)
            if result.ok:
                with open( fname, 'wb') as fp:
                    fp.write(result.data)
                LOG.debug('Created whitelist {} for {}'.format(fname, self.op_name))
                return fname
            else:
                LOG.error('Failed to encrypt to {}'.format(fname))
                return None
        else:
            with open(fname, 'w') as fp:
                fp.write(buff)
            LOG.debug('Created whitelist {} in {}'.format(fname, self.op_name))
            LOG.warning('Created unencrypted whitelist {} '.format(fname))
            return fname

    @perf
    def create_geo(self, geo_timestamps, reciptient):
        # function just for test purposes
        t = time.localtime()
        timestamp = time.strftime('%Y%m%d%H%M%S', t)
        fname = os.path.join(self.geo_path,'geo_'+timestamp+'.csv')
        if self.gpg_recipient and len(str(self.gpg_recipient).strip()) > 0:
            fname = fname + '.pgp'
            result = self.encrypt(geo_timestamps, reciptient)
            if result.ok:
                with open( fname, 'wb') as fp:
                    fp.write(result.data)
                LOG.debug('Created geo {} in {}'.format(fname, self.op_name))
                return fname
            else:
                LOG.error('Failed to encrypt to {}'.format(fname))
                return None
        else:
            with open(fname, 'w') as fp:
                fp.write(geo_timestamps)
            LOG.warning('Created unencrypted geo {}'.format(fname))
            LOG.debug('Created geo {} in {}'.format(fname, self.op_name))
            return fname

    @perf
    def encrypt(self, buff, recipient):
        return self.gpg.encrypt(buff, [recipient])

    @perf
    def decrypt(self, file_name):
        with open(file_name, "rb") as f:
            status = self.gpg.decrypt_file(f, passphrase=self.conf['gpg']['passphrase'])
            if status.ok:
                return True, status.data.decode()
        return False, None

    @perf
    def get_all_geo(self):
        geofiles = [os.path.join(self.geo_path, f) for f in os.listdir(self.geo_path) if os.path.isfile(os.path.join(self.geo_path, f))]
        rt = {}
        for f in geofiles:
            try:
                if f.lower().endswith('.pgp') or f.lower().endswith('.gpg'):
                    decrypted, data = self.decrypt(f)
                    if decrypted:
                        if not data or len(data) == 0:
                            rt[f] = []
                        else:
                            rt[f] = data.splitlines()
                        LOG.debug('Loaded {}'.format(f))
                    else:
                        LOG.error('Not possible to decrypt {}'.format(f))
                elif f.lower().endswith('csv'):
                    with open(f, "r") as fp:
                        rt[f] = fp.read().splitlines()
                        LOG.warning('Loaded unencrypted {}'.format(f))
            except Exception as e:
                LOG.error(e)
        return rt


class Keboola:

    def __init__(self, conf, check_and_create_paths = False):
        self.conf = conf
        self.op_name = 'keboola'
        self.home_path = conf[self.op_name]['home_path']
        check_path(self.home_path, check_and_create_paths)
        self.geoconfirm_path = os.path.join(self.home_path,'geoconfirm')
        check_path(self.geoconfirm_path, check_and_create_paths)
        self.geo_path =  os.path.join(self.home_path,'geo')
        check_path(self.geo_path, check_and_create_paths)
        self.geo_remove_path =  os.path.join(self.home_path,'to_remove')
        check_path(self.geo_remove_path, check_and_create_paths)


    @perf
    def create_geo(self, geo_list, operator_name):
        if not geo_list or len(geo_list) == 0:
            return None
        t = time.localtime()
        timestamp = time.strftime('%Y%m%d%H%M%S', t)
        fname = os.path.join(self.geo_path,'geo_'+timestamp+'_'+operator_name+'.csv')
        buff = "dt,period,lat,lon,precision_code,intensity,id,multipass\n"
        for x in geo_list:
            buff += x + '\n'
        with open( fname, 'w') as fp:
            fp.write(buff)
        LOG.debug('Created geo {} for keboola'.format(fname))
        return fname

    @staticmethod
    def list_from_ConsentWithdrawn_json(json_data):
        rt = []
        for x in json_data['payload']:
            rt.append(x['vMultipass'])
        return rt

    @perf
    def create_geo_remove(self, j_data):
        if not j_data:
            return None
        multipass_list = self.list_from_ConsentWithdrawn_json(j_data)
        if len(multipass_list) == 0:
            return None
        t = time.localtime()
        timestamp = time.strftime('%Y%m%d%H%M%S', t)
        fname = os.path.join(self.geo_remove_path,'geo_remove_'+timestamp+'.csv')
        buff = "multipass\n"
        for x in multipass_list:
            buff += x + '\n'
        with open( fname, 'w') as fp:
            fp.write(buff)
        LOG.debug('Created geo_remove {} for keboola'.format(fname))
        return fname


    @perf
    def create_geo_confirm(self, buff):
        fname = os.path.join(self.geoconfirm_path,'positive_list.csv')
        with open( fname, 'w') as fp:
            fp.write(buff)
        LOG.debug('Created geo confirm {} in keboola'.format(fname))
        return fname

    @perf
    def get_all_geo_confirm(self):
        confirm_files = [os.path.join(self.geoconfirm_path, f)
                        for f in os.listdir(self.geoconfirm_path)
                            if os.path.isfile(os.path.join(self.geoconfirm_path, f))]
        rt = {}
        for fname in confirm_files:
            with open(fname, 'r') as f:
                rt[fname] =  f.readlines()
            LOG.debug('Loaded {}'.format(fname))
        return rt
