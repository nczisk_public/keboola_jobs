import os
import requests_mock
import json
from sftp_jobs.telco import Keboola
from sftp_jobs.jobs import JobGeoRemove




geo_remove_response =  {
                    'warnings': [], 'errors': [], 'info': [],
                    'warning_count': 0, 'error_count': 0, 'payload_count': 2,
                    'payload':
                    [{'vMultipass': '8a345b42-46cc-48ab-9b6e-5779691fb824', 'vConsentGivenAt':'2020-03-09T00:00:00.000Z',
                      'vConsentWithDrawnAt':'2020-03-09T00:00:00.000Z' },
                     {'vMultipass': '8a345b42-46cc-48ab-9b6e-5wert3456345', 'vConsentGivenAt':'2020-03-09T00:00:00.000Z',
                      'vConsentWithDrawnAt':'2020-03-09T00:00:00.000Z' }]
               }


def test_geo_remove(config):
    tlist = ['multipass']
    tlist.extend(Keboola.list_from_ConsentWithdrawn_json(geo_remove_response))
    with requests_mock.Mocker() as m:
        m.post( config['api']['multipass_for_withdrawn_consents_api'] , text=json.dumps(geo_remove_response))
        job = JobGeoRemove(config)
        rt = job._run_implementation()
        assert rt
        assert os.path.exists(rt)
        with open(rt, "r") as fp:
            unecrypted = fp.read()
            assert unecrypted
            dlist = unecrypted.splitlines()
            assert dlist == tlist
