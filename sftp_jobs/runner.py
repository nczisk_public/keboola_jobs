import argparse
import json
import sys
from sftp_jobs.utils import *
from sftp_jobs.jobs import *
import pkg_resources

LOG = logging.getLogger(LOG_APP)


def parse_arguments(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str,
                        help='Configuration file',
                        required=True)
    parser.add_argument('--job', default='all', const='all', nargs='?',
                        choices=['all', 'whitelists', 'geo', 'geo_confirm', 'geo_remove'],
                        help='Job type to run',
                        required=True)
    return parser.parse_args(argv)


def get_my_version():
    try:
        return pkg_resources.get_distribution("keboola_jobs").version
    except:
        return '<not installed with setuptools>'


def main():
    try:
        args = parse_arguments(sys.argv[1:])
        with open(args.config, 'r') as fp:
            conf = json.load(fp)
        setup_log(conf)
        LOG.info('Softvare version:{}'.format(get_my_version()))
        if args.job == 'all':
            jbw = JobWhitelists(conf)
            jbw.run()
            jbg = JobGeo(conf)
            jbg.run()
            jbgc = JobGeoConfirm(conf)
            jbgc.run()
            jbgr = JobGeoRemove(conf)
            jbgr.run()
        elif args.job == 'whitelists':
            jbw = JobWhitelists(conf)
            jbw.run()
        elif args.job == 'geo':
            jbg = JobGeo(conf)
            jbg.run()
        elif args.job == 'geo_confirm':
            jbgc = JobGeoConfirm(conf)
            jbgc.run()
        elif args.job == 'geo_remove':
            jbgc = JobGeoRemove(conf)
            jbgc.run()
        exit(0)
    except Exception as e:
        LOG.error(e, exc_info=True)
        exit(1)


if __name__ == '__main__':
    main()
